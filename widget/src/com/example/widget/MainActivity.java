package com.example.widget;
 
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

public class MainActivity extends AppWidgetProvider {
	
	public static String ACTION_TOOGLE_HEATING = "toogleHeating";
	public static String ACTION_MORE_CONSIGNA = "moreConsigna";
	public static String ACTION_LESS_CONSIGNA = "lessConsigna";
	
	
	@Override
	public void onEnabled (Context context){
		//To setting the default parameters
  		SharedPreferences settings = context.getSharedPreferences("persistencia", 0);
		SharedPreferences.Editor persistencia = settings.edit();
		persistencia.putString("url", "http://puigverd.org");
		
		Log.e("1","onEnabled");
		// Variable for manage the concurrence
		persistencia.putInt("concurrentes", 0);
		persistencia.commit();    
		Log.e("1","onEnabled");
	}//FIn constructora
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		
		final int N = appWidgetIds.length;
			 
		// Perform this loop procedure for each App Widget that belongs to this provider
		for (int i = 0; i < N; i++) {
			int appWidgetId = appWidgetIds[i];
			
			Intent intent = new Intent(context, MainActivity.class);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
			RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.activity_main);
			
			//set de event for activate or desactevate the heatting system
			intent = new Intent(context, MainActivity.class);
			intent.setAction(ACTION_TOOGLE_HEATING);
			pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
			views.setOnClickPendingIntent(R.id.imgFire, pendingIntent);
			
			//set the event for more consigna
			intent = new Intent(context, MainActivity.class);
			intent.setAction(ACTION_MORE_CONSIGNA);
			pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
			views.setOnClickPendingIntent(R.id.imageMore, pendingIntent);
			
			//set the event for less consigna
			intent = new Intent(context, MainActivity.class);
			intent.setAction(ACTION_LESS_CONSIGNA);
			pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
			views.setOnClickPendingIntent(R.id.imageLess, pendingIntent);
			
			// To show the ConfActivity
			Intent intento = new Intent(context, ConfActivity.class);
			intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
			PendingIntent pendingIntent2 = PendingIntent.getActivity(context, 0,intento, PendingIntent.FLAG_UPDATE_CURRENT);
			views.setOnClickPendingIntent(R.id.android_widget, pendingIntent2);
			
			// Tell the AppWidgetManager to perform an update on the current app widget
			appWidgetManager.updateAppWidget(appWidgetId, views);
		}
	}//Fin onUpdate
  
  	@Override
	public void onReceive(Context context, Intent intent) {
  		super.onReceive(context, intent);
  		if (intent.getAction().equals(ACTION_TOOGLE_HEATING)) {
  			// To enable the persistent data in widget envoirement 
  			SharedPreferences settings = context.getSharedPreferences("persistencia", 0);
  			SharedPreferences.Editor persistencia = settings.edit();
  			
  			// To change the imgFire 
  			RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.activity_main);
  			
  			if ( settings.getBoolean("estado", false) ){
  				views.setImageViewResource(R.id.imgFire, R.drawable.fire_red);
  				persistencia.putBoolean("estado", false);
  	  	      	persistencia.commit();
  			}
  			else{
  				views.setImageViewResource(R.id.imgFire, R.drawable.fire);
  				persistencia.putBoolean("estado", true);
	  	      	persistencia.commit();
  			}
  			
  			// To refresh the widget
  			ComponentName myWidget = new ComponentName(context,MainActivity.class);
  			AppWidgetManager manager = AppWidgetManager.getInstance(context);
  			manager.updateAppWidget(myWidget, views);
  		}
			
  		if (intent.getAction().equals(ACTION_MORE_CONSIGNA)) {
  			//recupero el valor de la consigna
  			SharedPreferences settings = context.getSharedPreferences("persistencia", 0);
  			Integer consigna = settings.getInt("consigna", 0);	
  			consigna = consigna+1;
  			// To update a label
  			RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.activity_main);
  			views.setTextViewText(R.id.textViewConsigna, String.valueOf(consigna) );
  			// To refresh the widget
  			ComponentName myWidget = new ComponentName(context,MainActivity.class);
  			AppWidgetManager manager = AppWidgetManager.getInstance(context);
  			manager.updateAppWidget(myWidget, views);
  			
  			//guardo el nuevo valor de la consigna
  			SharedPreferences.Editor persistencia = settings.edit();
  			persistencia.putInt("consigna", consigna);
	  	    persistencia.commit();
	  	    
	  	    //wait 5 seconds for send the new consiga if click another time beffore this 5 second i cancel this wait
	  	    remoteUpdate(context,consigna );
  		}
  			
  		if (intent.getAction().equals(ACTION_LESS_CONSIGNA)) {
  			//recupero el valor de la consigna
  			SharedPreferences settings = context.getSharedPreferences("persistencia", 0);
  			Integer consigna = settings.getInt("consigna", 0);	
  			consigna = consigna-1 ;
  			// To update a label
  			RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.activity_main);
  			views.setTextViewText(R.id.textViewConsigna, String.valueOf(consigna));
  			// To refresh the widget
  			ComponentName myWidget = new ComponentName(context,MainActivity.class);
  			AppWidgetManager manager = AppWidgetManager.getInstance(context);
  			manager.updateAppWidget(myWidget, views);
  			
  			//guardo el nuevo valor de la consigna
  			SharedPreferences.Editor persistencia = settings.edit();
  			persistencia.putInt("consigna", consigna);
	  	    persistencia.commit();
	  	    
	  	    //wait 5 seconds for send the new consiga if click another time beffore this 5 second i cancel this wait
	  	    remoteUpdate(context,consigna);
  		}  		
  	}//Fin onReceive
  	
  	public void remoteUpdate(Context context,Integer consigna){
  		Log.e("1","Lanzo thread1");
  		
  		// To manage the concurrence
  		SharedPreferences settings = context.getSharedPreferences("persistencia", 0);
		SharedPreferences.Editor persistencia = settings.edit();
		Integer concurrentes = settings.getInt("concurrentes", 0);
		concurrentes = concurrentes+1;
		persistencia.putInt("concurrentes", concurrentes);
		persistencia.commit();    
  		
  		Handler myHandler = new Handler();
  		MyRunnable aaa = new MyRunnable(context, consigna);
  		
  		myHandler.removeCallbacks(null);
  		myHandler.removeCallbacksAndMessages (null);
  		myHandler.postDelayed(aaa, 5000);

  		Log.e("4","FIN 4");
  	}//Fin remoteUpdate
  	
  	public class MyRunnable implements Runnable {
  		
  		private int consigna;
  		private Context context;
  		
  		public MyRunnable(Context _context,Integer _consigna ) {
  		    this.consigna = _consigna;
  			this.context = _context;
  		}//Fin constructora
  			
  		public void run(){
  		   	Log.e("2","2 Ejecuto thread");
  		   	// To manage the persisntece
  		   	SharedPreferences settings = context.getSharedPreferences("persistencia", 0);
  		   	SharedPreferences.Editor persistencia = settings.edit();
  		   	Integer concurrentes = settings.getInt("concurrentes", 0);
  		   	concurrentes = concurrentes-1;
  		   	persistencia.putInt("concurrentes", concurrentes);
  		   	persistencia.commit();    
  		    Log.e("3","Numeros dde concurrentes"+concurrentes);
  		   	if (concurrentes<=0){
		  	    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		  	    StrictMode.setThreadPolicy(policy);
		  	    //your codes here
		  	    Comunication myComunication = new Comunication();
		  	  	Estado estado = myComunication.sendMsg(consigna);
		  	  	
		  	  	if( estado.error == "Error"){
		  	  		Toast.makeText(context, "Error de red: ",Toast.LENGTH_LONG).show();
		  	  		
		  	  	}
		  	  	else{
			  	  	//update the widget data 
			  	   	AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
			  	   	RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.activity_main);
			  	   	ComponentName thisWidget = new ComponentName(context, MainActivity.class);
			  	   	remoteViews.setTextViewText(R.id.textViewActualizado, estado.time);
			  	   	remoteViews.setTextViewText(R.id.textViewTemperatura, estado.temperatura);
			  	   	appWidgetManager.updateAppWidget(thisWidget, remoteViews);//*/
		  	  	}  	
		  	   	Log.e("3","unicooooooooooooo");
  		   	}
	  	  	Log.e("3","3 Finalizo thread");
  		}//Fin run
  	}//Fin clase ;
  		 
}//Fin clase MyRunnable