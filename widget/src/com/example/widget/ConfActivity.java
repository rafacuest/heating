package com.example.widget;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.RemoteViews;
import android.widget.TextView;

public class ConfActivity extends Activity {
	
	private SharedPreferences settings;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_conf);
		 //--- find both the buttons---
        Button sButton = (Button) findViewById(R.id.button1);
        
        this.settings = this.getApplicationContext().getSharedPreferences("persistencia", 0);
		String url = settings.getString("url", "http://puigverd.org");
		
		//Set init variables 
		final TextView textViewToChange = (TextView) findViewById(R.id.url);
		textViewToChange.setText(url);
        
        
        
        // -- register click event with first button ---
        sButton.setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {        	  
           //To make the call to webservice with a thread
           int SDK_INT = android.os.Build.VERSION.SDK_INT;
           if (SDK_INT > 8){
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                //your codes here
                	
              
        		SharedPreferences.Editor persistencia = settings.edit();
        		
        		final TextView textViewToChange = (TextView) findViewById(R.id.url);
        		
        		persistencia.putString("url", (String)textViewToChange.getText());
        		persistencia.commit();

                
        	    }
           }
        });
	}//Fin onCreate
}
