package com.example.widget;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.SharedPreferences;
import android.util.Log;

public class Comunication {
	public Estado estado;
	public String url;
	
	public Comunication(){
		 
	}//Fin constructora
	
	public Estado sendMsg(Integer consigna) {
		estado = new Estado();
	    try {

	        HttpParams httpParams = new BasicHttpParams();
	        HttpConnectionParams.setConnectionTimeout(httpParams,10000);
	        HttpConnectionParams.setSoTimeout(httpParams, 10000);
	        //
	        HttpParams p = new BasicHttpParams();
	        
	        // Instantiate an HttpClient
	        HttpClient httpclient = new DefaultHttpClient(p);
	        String url = "http://puigverd.org/heating/webservice.php";
	        HttpPost httppost = new HttpPost(url);
	        
	        // Instantiate a GET HTTP method
	        try {
	            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
	            nameValuePairs.add(new BasicNameValuePair("user", "rafa"));
	            nameValuePairs.add(new BasicNameValuePair("pasword", "rafa"));
	            nameValuePairs.add(new BasicNameValuePair("consigna", String.valueOf(consigna)));
	            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	            ResponseHandler<String> responseHandler = new BasicResponseHandler();
	            String responseBody = httpclient.execute(httppost,responseHandler);
	            
	            JSONObject jObj = new JSONObject(responseBody);
	            
	            JSONArray jArr = jObj.getJSONArray("state");
                JSONObject obj = jArr.getJSONObject(0);
                
                estado.temperatura = obj.getString("temperatura");
                estado.consigna_temperatura = obj.getString("consigna_temperatura");
                estado.consigna_actuaci�n = obj.getString("consigna_actuacion");
                estado.estado_circuito = obj.getString("estado_circuito");
                estado.accionar_circuito = obj.getString("accionar_circuito");
                estado.time = obj.getString("time");
                estado.error = "";
	            return estado;
	            
	        } catch (ClientProtocolException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	            estado.error = "Error";
	            Log.i("Error","");
		    	return estado;
	        } catch (IOException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	            Log.i("Error","");
	            estado.error = "Error";
		    	return estado;
	        }
	        // Log.i(getClass().getSimpleName(), "send  task - end");

	    } catch (Throwable t) {
	    	Log.i("Request failed: " + t.toString(),"");
	    	estado.error = "Error";
	    	return estado;
	    }
	}//Fin sendMsg
	
}
